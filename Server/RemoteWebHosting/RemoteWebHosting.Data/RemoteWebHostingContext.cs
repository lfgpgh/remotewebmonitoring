﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteWebHosting.Data
{
    public class RemoteWebHostingContext : DbContext
    {
        public virtual IDbSet<WebTest> WebTest { get; set; }
        public virtual IDbSet<HarData> HarData { get; set; }
        public virtual IDbSet<HarResult> HarResult { get; set; }


        public RemoteWebHostingContext(): this("DefaultConnection")
        {
            Database.CommandTimeout = 180;
        }

        public RemoteWebHostingContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        public int SaveContext()
        {
            DateTime now = DateTime.UtcNow;
            ChangeTracker.Entries().Where(o => o.Entity is IRequired)
                .Select(o => o.Cast<IRequired>()).ToList()
                .ForEach(o => UpdateRequiredFields(o.Entity, now, o.State));
            return SaveChanges();
        }

        public void UpdateRequiredFields(IRequired required,  DateTime now, EntityState state)
        {
            if (state == EntityState.Added)
            {
                required.CreatedOn = now;
            }

            if (state == EntityState.Added || state == EntityState.Modified)
            {
                required.UpdatedOn = now;
            }
        }
    }
}
