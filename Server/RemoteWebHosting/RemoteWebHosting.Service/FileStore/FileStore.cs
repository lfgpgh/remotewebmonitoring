﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteWebHosting.Service.FileStore
{
    public class FileStore:IFileStore
    {
        private readonly string _storePath;

        public FileStore(string storePath)
        {
            if (!Directory.Exists(storePath))
            {
                throw new ArgumentException("File store path does not exist: \"" + storePath + "\"", "storePath");
            }
            _storePath = storePath;
        }

        private string FilePathFor(string fileName)
        {
            return Path.Combine(_storePath, fileName);
        }

        public Task<string> GetFilePath(string fileName)
        {
            return Task.Run(() => FilePathFor(fileName));
        }

        public Task<byte[]> ReadAllBytes(string fileName)
        {
            return Task.Run(() => File.ReadAllBytes(FilePathFor(fileName)));
        }

        public Task<bool> WriteAllBytes(string fileName, byte[] data)
        {
            return Task.Run(() =>
            {
                var filePath = FilePathFor(fileName);
                File.WriteAllBytes(filePath, data);
                return File.Exists(filePath);
            });
            
        }

        public Task<Stream> Read(string fileName)
        {
            return Task.Run(() =>
            {
                Stream ms = new MemoryStream();
                using (var fs = new FileStream(FilePathFor(fileName), FileMode.Open))
                {
                    fs.CopyTo(ms);
                    ms.Position = 0;
                }
                return ms;
            });
            
        }

        public Task<bool> Write(string fileName, Stream data)
        {
            return Task.Run(() =>
            {
                var filePath = FilePathFor(fileName);
                using (var fs = new FileStream(filePath, FileMode.Create))
                {
                    data.Position = 0;
                    data.CopyTo(fs);

                    return File.Exists(filePath);
                } 
            });
        }

        public Task<bool> Delete(string fileName)
        {
            return Task.Run(() =>
            {
                var filePath = FilePathFor(fileName);
                File.Delete(filePath);
                return !File.Exists(filePath);
            });
            
        }

        public async Task<bool> CopyTo(string fromFileName, IFileStore destinationStore, string destinationFileName)
        {
            var data = await Read(fromFileName);
            return await destinationStore.Write(destinationFileName, data);
        }

        public Task<bool> Exists(string fileName)
        {
            return Task.Run(() =>
            {
                var filePath = FilePathFor(fileName);
                return File.Exists(filePath);
            });
            
        }

        public Task<bool> WriteAllText(string fileName, string data)
        {
            return Task.Run(() =>
            {
                var filePath = FilePathFor(fileName);
                File.WriteAllText(filePath, data);
                return File.Exists(filePath);
            });
        }
    }
}
