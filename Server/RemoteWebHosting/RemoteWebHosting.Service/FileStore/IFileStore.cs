﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteWebHosting.Service.FileStore
{
    public interface IFileStore
    {
        Task<string> GetFilePath(string fileName);
        Task<byte[]> ReadAllBytes(string fileName);
        Task<bool> WriteAllBytes(string fileName, byte[] data);
        Task<Stream> Read(string fileName);
        Task<bool> Write(string fileName, Stream data);
        Task<bool> Delete(string fileName);
        Task<bool> CopyTo(string fromFileName, IFileStore destinationStore, string destinationFileName);
        Task<bool> Exists(string fileName);
        Task<bool> WriteAllText(string fileName, string data);
    }
}
