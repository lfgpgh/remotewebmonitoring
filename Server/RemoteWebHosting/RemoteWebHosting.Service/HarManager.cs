﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RemoteWebHosting.Core.DTO;
using RemoteWebHosting.Core.JsonDto;
using RemoteWebHosting.Data;

namespace RemoteWebHosting.Service
{
    public class HarManager
    {
        public bool SaveHarData(HarDataDto harData)
        {
            using (var context = new RemoteWebHostingContext())
            {
                HarData har = new HarData
                {
                    Blob = harData.Blob
                };
                context.HarData.Add(har);
                return context.SaveChanges()>0;
            }
        }

        public Guid SaveHarResult(HarResultDto harResult)
        {
            using (var context = new RemoteWebHostingContext())
            {
                HarData harData = new HarData();
                HarResult harRes = new HarResult
                {
                    Dns = harResult.Dns,
                    Ip = harResult.Ip,
                    NodeId = harResult.NodeId,
                    NodeName = harResult.NodeName,
                    Ping = harResult.Ping,
                    TestTime = harResult.TestTime,
                    TotalSize = harResult.TotalSize,
                    TotalTime = harResult.TotalTime,
                    HarData = harData
                };
                if (harResult.To != null)
                {
                    harRes.To = new Geo
                    {
                        Latitude = harResult.To.Latitude,
                        Longitude = harResult.To.Longitude
                    };
                }
                if (harResult.From != null)
                {
                    harRes.From = new Geo
                    {
                        Latitude = harResult.From.Latitude,
                        Longitude = harResult.From.Longitude
                    };
                }

                context.HarResult.Add(harRes);
                context.HarData.Add(harData);
                context.SaveContext();
                return harData.Id;
            }
        }

        public HarDataDto GetHarData(Guid id)
        {
            HarDataDto harData = null;
            using (var context = new RemoteWebHostingContext())
            {
                var data = context.HarData.FirstOrDefault(_ => _.Id == id);
                if (data != null)
                {
                    harData = new HarDataDto
                    {
                        Id = data.Id,
                        Blob = data.Blob
                    };
                }
            }
            return harData;
        }

        public HarResultDto GetHarResult(Guid id)
        {
            HarResultDto harResult = null;
            using (var context = new RemoteWebHostingContext())
            {
                var result = context.HarResult.FirstOrDefault(_ => _.Id == id);
                harResult = ParseHarResult(result);
            }
            return harResult;
        }

        #region [HarData Helpers]
        public HarDataDto ParseHarData(HarData data)
        {
            if (data == null)
                return null;
            return new HarDataDto
            {
                Id = data.Id,
                Blob = data.Blob
            };
        }

        public HarData ParseHarData(HarDataDto data)
        {
            if (data == null)
                return null;
            return new HarData
            {
                Id = data.Id,
                Blob = data.Blob
            };
        }
        #endregion

        #region [HarResult Helpers]

        public HarResultDto ParseHarResult(HarResult result)
        {
            if (result == null)
                return null;
            return new HarResultDto
            {
                HarData = ParseHarData(result.HarData),
                Id = result.Id,
                From = ParseGeo(result.From),
                Dns = result.Dns,
                Ip = result.Ip,
                NodeId = result.NodeId,
                NodeName = result.NodeName,
                Ping = result.Ping,
                //Test = result.WebTest,
                TestTime = result.TestTime,
                To = ParseGeo(result.To),
                TotalSize = result.TotalSize,
                TotalTime = result.TotalTime
            };
        }

        public HarResult ParseHarResult(HarResultDto result)
        {
            if (result == null)
                return null;
            return new HarResult
            {
                HarData = ParseHarData(result.HarData),
                Id = result.Id,
                From = ParseGeo(result.From),
                Dns = result.Dns,
                Ip = result.Ip,
                NodeId = result.NodeId,
                NodeName = result.NodeName,
                Ping = result.Ping,
                //Test = result.WebTest,
                TestTime = result.TestTime,
                To = ParseGeo(result.To),
                TotalSize = result.TotalSize,
                TotalTime = result.TotalTime
                
            };
        }
        #endregion

        #region [WebTest Helpers]
        
        #endregion

        #region [Geo Helpers]
        public GeoDto ParseGeo(Geo geo)
        {
            if (geo == null)
                return null;
            return new GeoDto
            {
                 Latitude = geo.Latitude,
                 Longitude = geo.Longitude
            };
        }

        public Geo ParseGeo(GeoDto geoDto)
        {
            if (geoDto == null)
                return null;
            return new Geo
            {
                Latitude = geoDto.Latitude,
                Longitude = geoDto.Longitude
            };
        }
        #endregion
    }
}
