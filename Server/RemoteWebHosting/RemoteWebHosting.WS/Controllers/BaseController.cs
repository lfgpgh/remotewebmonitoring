﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using RemoteWebHosting.Service.FileStore;

namespace RemoteWebHosting.WS.Controllers
{
    public class BaseController:ApiController
    {
        private IFileStore _fileStore;

        public IFileStore FileStore
        {
            get
            {
                return _fileStore ??
                       (_fileStore = new FileStore(ConfigurationManager.AppSettings["FileStoreLocation"]));
            }
        }
    }
}