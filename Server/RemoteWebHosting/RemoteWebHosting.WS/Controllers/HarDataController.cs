﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.WebPages;
using RemoteWebHosting.Service;

namespace RemoteWebHosting.WS.Controllers
{
    public class HarDataController : BaseController
    {
        [HttpPost]
        public async Task<HttpResponseMessage> SaveHarDataJson()
        {
            //TODO Zach: Fix to save data to database
            var json = await Request.Content.ReadAsStringAsync();
            var fileName = string.Format("{0}.har",Guid.NewGuid());
            var success = await FileStore.WriteAllText(fileName, json);

            return success ? Request.CreateResponse(HttpStatusCode.OK) : Request.CreateErrorResponse(HttpStatusCode.InternalServerError, string.Format("Error saving file: {0}", fileName));

        }
        //[HttpPost]
        //public async Task<HttpResponseMessage> SaveHarData([FromUri]string id)
        //{
        //    var fileName = string.Format("{0}.har", id);
        //    var document = Request.Content.ReadAsStreamAsync().Result;
        //    var success = await FileStore.Write(fileName, document);

        //    return success ? Request.CreateResponse(HttpStatusCode.OK) : Request.CreateErrorResponse(HttpStatusCode.InternalServerError, string.Format("Error saving file: {0}", fileName));
        //}

        [HttpGet]
        public async Task<HttpResponseMessage> GetHarData([FromUri] string id)
        {
            var fileName = string.Format("{0}.har", id);

            var data = await FileStore.Read(fileName);
            HttpResponseMessage result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(data);
            result.Content.Headers.ContentDisposition =
                new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = fileName
                };
            return result;
        }
    }
}